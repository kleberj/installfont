This work consists of the following files:

-- scripts --

installfont
installfont-tl

-- Documentation --

installfont.tex
installfont.pdf

-- Misc. Files --

LICENSE
README
manifest.txt